package com.here.hellomap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

import org.json.JSONObject;

class FlutterViewActivity extends AppCompatActivity {

    static final String CHANNEL = "net.ngocdung.afeandroid/data";

    void startActivity(MainActivity context) {
        Intent intent = new Intent(context, FlutterViewActivity.class);
//            intent.putExtra("first", first);
//            intent.putExtra("second", second)
        context.startActivityForResult(intent, 100);
    }

}