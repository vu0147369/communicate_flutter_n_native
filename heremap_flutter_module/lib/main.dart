import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('samples.flutter.dev/battery');

  // Get battery level.
  String _batteryLevel = '';

  var long = 0;

  var lat = 0;

  _MyHomePageState() {
    platform.setMethodCallHandler(_receiveFromNative);
  }

  Future<void> _sendLocationToNative() async {
    String batteryLevel;
    try {
      Map<String, dynamic> resultMap = Map();
      resultMap['long'] = "0";
      resultMap['lat'] = "0";

      platform.invokeMethod('getLongLat', resultMap.toString());
      batteryLevel = 'getLongLat = $resultMap';
      setState(() {
        _batteryLevel = batteryLevel;
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Text("Alert Dialog title"),
              content: new Text("getLongLat = $long, $lat"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      });
    } on PlatformException catch (e) {
      batteryLevel = "Failed to getLongLat: '${e.message}'.";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RaisedButton(
              child: Text('set location'),
              onPressed: _sendLocationToNative,
            ),
            Text(_batteryLevel),
          ],
        ),
      ),
    );
  }

  Future _receiveFromNative(MethodCall call) {
    try {
      print(call.method);

      if (call.method == "formNativeToFlutter") {
        final String data = call.arguments;
        print(call.arguments);

        final jsonData = jsonDecode(data);

        long = jsonData['long'];
        lat = jsonData['lat'];

        setState(() {
          _batteryLevel = 'getLongLat = $long, $lat';
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text("Alert Dialog title"),
                content: new Text("getLongLat = $long, $lat"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        });
      }
    } on PlatformException catch (e) {
      print(e.message);
    }
  }
}
